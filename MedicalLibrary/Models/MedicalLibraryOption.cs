﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MedicalLibrary.Models
{
    public partial class MedicalLibraryOption
    {
        public int Id { get; set; }
        public string DropDownName { get; set; }
    }

    public class MedicalLibraryOptions
    {
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }
}
