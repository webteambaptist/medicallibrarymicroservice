﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalLibrary.Models;

namespace MedicalLibrary.Controllers
{
    [Route("api/medicallibrary")]
    [ApiController]
    public class MedicalLibraryController : ControllerBase
    {
        private PhysiciansPortalContext _dbContext;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public MedicalLibraryController(PhysiciansPortalContext dbContext)
        {
            _dbContext = dbContext;
            var nLogConfig = new NLog.Config.LoggingConfiguration();
            var loggerFile = new NLog.Targets.FileTarget("Logfile") { FileName = $"Logs\\MedicalLibrarysMicroservice-{DateTime.Now:MM-dd-yyyy}.Logger" };
            nLogConfig.AddRule(LogLevel.Info, LogLevel.Fatal, loggerFile);
            LogManager.Configuration = nLogConfig;
        }
        // GET: api/medicallibrary
        [HttpGet]
        public string Get()
        {
            var msg = "Starting Medical Library Microservice...";
            return msg;
        }

        [Route("getoptions")]
        [HttpGet] 
        public ActionResult getOptions()
        {
            try 
            {
                var medicalLibraryOptions = _dbContext.MedicalLibraryOptions.ToList();

                var mloList = new List<MedicalLibraryOptions>();

                foreach (var option in medicalLibraryOptions)
                {
                    var optionValues = _dbContext.MedicalLibraryOptionValues.Where(x => x.OptionsId == option.Id)
                        .ToList();

                    foreach (var value in optionValues)
                    {
                        var mlov = new MedicalLibraryOptions()
                        {
                            OptionName = option.DropDownName,
                            OptionValue = value.Value
                        };
                        mloList.Add(mlov);
                    }
                }
                return Ok(mloList);
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred in getoptions :: " + ex.Message);
                return BadRequest();
            }
        }
    }
}
